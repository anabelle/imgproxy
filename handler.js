'use strict';

const axios = require('axios');

module.exports.hello = async (event, context, callback) => {
	var url = "http://siata.gov.co/ultimasFotosCamaras/ultimacam_ituango_descarga.jpg";
	var headers = { 'Content-Type': 'image/jpeg' };
	var body;

	const imageBase64 = await axios.get(url, { responseType: 'arraybuffer' })
    .then(
		(response) => {
			body = Buffer.from(response.data, 'binary').toString('base64');
			headers = response.headers;
		}
	);
	return {
		statusCode: 200,
		headers: headers,
		body: body,
		isBase64Encoded: true, //the most important part
	}
};

module.exports.embalse = async (event, context, callback) => {
	var url = "http://siata.gov.co/ultimasFotosCamaras/ultimacam_ituango_embalse.jpg";
	var headers = { 'Content-Type': 'image/jpeg' };
	var body;

	const imageBase64 = await axios.get(url, { responseType: 'arraybuffer' })
    .then(
		(response) => {
			body = Buffer.from(response.data, 'binary').toString('base64');
			headers = response.headers;
		}
	);
	return {
	  statusCode: 200,
	  headers: headers,
	  body: body,
	  isBase64Encoded: true, //the most important part
	}
};

module.exports.subestacion = async (event, context, callback) => {
	var url = "http://siata.gov.co/ultimasFotosCamaras/ultimacam_ituango_sub_estacion_500.jpg";
	var headers = { 'Content-Type': 'image/jpeg' };
	var body;

	const imageBase64 = await axios.get(url, { responseType: 'arraybuffer' })
    .then(
		(response) => {
			body = Buffer.from(response.data, 'binary').toString('base64');
			headers = response.headers;
		}
	);
	return {
	  statusCode: 200,
	  headers: headers,
	  body: body,
	  isBase64Encoded: true, //the most important part
	}
};
